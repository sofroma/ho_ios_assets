package com.bergen.ho.iosassets;

import com.google.gson.annotations.SerializedName;

public class AssetProperties {

    @SerializedName("on-demand-resource-tags")
    String[] onDemandResourceTags;

    public AssetProperties(String[] tags) {
        onDemandResourceTags = tags;
    }
}
