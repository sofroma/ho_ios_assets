package com.bergen.ho.iosassets;

import com.google.gson.annotations.SerializedName;

/**
 * Created by romansofronov on 21.03.18.
 */
public class AssetData {

	String idiom = "universal";

	String filename;

	@SerializedName("universal-type-identifier")
	String universalTypeIdentifier = "public.png";

	String memory;

	AssetData(String filename) {
		this.filename = "s" + "." + filename;
	}

	AssetData(String filename, String memory) {
		this.filename = "l" + "." + filename;
		this.memory = memory;
	}

	AssetData(String filename, String tag, String memory) {
		this.filename = tag + "." + filename;
		this.memory = memory;
	}

}
