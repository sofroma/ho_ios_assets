package com.bergen.ho.iosassets.config;

import com.google.gson.annotations.SerializedName;

/**
 * Created by romansofronov on 22.03.18.
 */
public class Config {
	public String smallAssetsPath;
	public String origAssetsPath;
	public String targetDirPath;

	@SerializedName("on-demand-resources")
	public OnDemandResources onDemandResources;

}
