package com.bergen.ho.iosassets;

import java.io.File;

/**
 * Created by romansofronov on 21.03.18.
 */
public class Asset {

	AssetEntry info = new AssetEntry();
	AssetData[] data;

	Asset(File smallAsset, File origAsset) {
		data = new AssetData[3];
		data[0] = new AssetData(smallAsset.getName());
		data[1] = new AssetData(smallAsset.getName(), "s", "1GB");
		data[2] = new AssetData(origAsset.getName(), "l", "2GB");
	}

	Asset() {

	}

}
