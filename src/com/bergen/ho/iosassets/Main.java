package com.bergen.ho.iosassets;

import com.bergen.ho.iosassets.config.Config;
import com.bergen.ho.iosassets.config.TagEntry;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;

/**
 * Created by romansofronov on 20.03.18.
 */
public class Main {

	private String smallAssetsPath;
	private String origAssetsPath;
	private String targetDirPath;

	public static void main(String[] args) {
		Config config = null;
		try {
			String json = new String(Files.readAllBytes(Paths.get("Config.json")));
			config = new Gson().fromJson(json, Config.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(config != null) {
			new Main().run(config);
		}
	}

	private void run(Config config) {
		smallAssetsPath = config.smallAssetsPath;
		origAssetsPath = config.origAssetsPath;
		targetDirPath = config.targetDirPath;

		HashMap<String, File> smallAssets = new HashMap<>();
		HashMap<String, File> origAssets = new HashMap<>();

		File smallAssetsDir = new File(smallAssetsPath);
		File origAssetsDir = new File(origAssetsPath);

		List<File> smallAssetsList = new ArrayList<>();
		List<File> origAssetsList = new ArrayList<>();

		getAllFiles(smallAssetsDir, smallAssetsList);
		getAllFiles(origAssetsDir, origAssetsList);

		for(File file : smallAssetsList) {
			String path = getPath(file);
			smallAssets.put(path, file);
		}

		for(File file : origAssetsList) {
			String path = getPath(file);
			origAssets.put(path, file);
		}

		prepareAssets(smallAssets, origAssets, config);
	}

	private void getAllFiles(File dir, List<File> fileList) {
		File[] files = dir.listFiles();
		if(files != null) {
			for (File file : files) {
				if(!file.isDirectory())
					fileList.add(file);
				if (file.isDirectory()) {
					getAllFiles(file, fileList);
				}
			}
		}
	}

	private void getAllDirs(File dir, List<File> dirList) {
		File[] files = dir.listFiles();
		if(files != null) {
			for (File file : files) {
				if(file.isDirectory()) {
					dirList.add(file);
					getAllDirs(file, dirList);
				}
			}
		}
	}

	private void getDirChildDirs(File dir, List<File> dirList) {
		File[] files = dir.listFiles();
		if(files != null) {
			for(File file : files) {
				if(file.isDirectory()) {
					dirList.add(file);
				}
			}
		}
	}

	private void prepareAssets(HashMap<String, File> smallAssets, HashMap<String, File> origAssets, Config config) {
		HashMap<String, Asset> assets = new HashMap<>();
		for(Map.Entry<String, File> entry : origAssets.entrySet()) {
			String path = entry.getKey();
			File origAsset = entry.getValue();
			File smallAsset = smallAssets.get(path);

			if(origAsset != null && smallAsset != null) {
				Asset asset = new Asset(smallAsset, origAsset);
				assets.put(path, asset);
			} else {
				//System.out.println("sasd");
			}

		}
		placeAssets(assets, smallAssets, origAssets);
		//placeFolderInfos();
		List<File> dirs = new ArrayList<>();
		getDirChildDirs(new File(targetDirPath), dirs);

		placeFolderInfos2(dirs);
		placeFolderInfosWithTags(dirs, config);
	}

	private void placeAssets(HashMap<String, Asset> assets, HashMap<String, File> smallAssets, HashMap<String, File> origAssets) {
		Gson gson = new Gson();
		File targetDir = new File(targetDirPath);
		if(!targetDir.exists()) {
			targetDir.mkdirs();
		}
		for(Map.Entry<String, Asset> entry : assets.entrySet()) {
			String assetPath = entry.getKey();
			Asset asset = entry.getValue();
			File assetDir = new File(targetDir, assetPath).getParentFile();
			//String assetName = getNameWithExt(assetPath);
			String assetName = assetPath.replace("/", ".");
			File assetCatalogDir = new File(assetDir, assetName + ".dataset");
			if(!assetCatalogDir.exists()) {
				boolean isDirCreated = assetCatalogDir.mkdirs();
			}
			File assetCatalogFile = new File(assetCatalogDir, "Contents.json");
			try {
				FileOutputStream fos = new FileOutputStream(assetCatalogFile);
				String json = gson.toJson(entry.getValue());
				fos.write(json.getBytes());
				fos.close();
				Files.copy(Paths.get(smallAssetsPath + "/" + assetPath), Paths.get(assetCatalogDir.getPath() + "/" + asset.data[0].filename), StandardCopyOption.REPLACE_EXISTING);
				Files.copy(Paths.get(origAssetsPath + "/" + assetPath), Paths.get(assetCatalogDir.getPath() + "/" + asset.data[2].filename), StandardCopyOption.REPLACE_EXISTING);
				//Files.copy(Paths.get("1.data"), Paths.get(assetCatalogDir.getPath() + "/" + asset.data[2].filename), StandardCopyOption.REPLACE_EXISTING);

			} catch (IOException e){
				e.printStackTrace();
				System.exit(1);
			}
		}
	}

	private void placeFolderInfos() {
		List<File> dirList = new ArrayList<>();
		File dir = new File(targetDirPath);
		getAllDirs(dir, dirList);
		Gson gson = new Gson();
		Asset asset = new Asset();
		for(File d : dirList) {
			File c = new File(d, "Contents.json");
			if(!c.exists()) {
				try {
					FileOutputStream fos = new FileOutputStream(c);
					String json = gson.toJson(asset);
					fos.write(json.getBytes());
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(2);
				}
			}
		}
	}

	private void placeFolderInfos2(List<File> dirs) {
		List<File> targetDirs = new ArrayList<>();
		for(File dir : dirs) {
			getAllDirs(dir, targetDirs);
		}
		placeFolderInfoIntoTheseFolders(targetDirs);
	}

	private void placeFolderInfoIntoTheseFolders(List<File> dirList) {
		Gson gson = new Gson();
		for(File d : dirList) {
			Asset asset = new Asset();
			File c = new File(d, "Contents.json");
			if(!c.exists()) {
				try {
					FileOutputStream fos = new FileOutputStream(c);
					String json = gson.toJson(asset);
					fos.write(json.getBytes());
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(2);
				}
			}
		}
	}

	private void placeFolderInfosWithTags(List<File> dirs, Config config) {
		if(config.onDemandResources != null && config.onDemandResources.tags != null) {
			Gson gson = new Gson();
			for(File d : dirs) {
				String[] tags = null;
				List<String> tempTags = new ArrayList<>();
				for(TagEntry entry : config.onDemandResources.tags) {
					for(String group : entry.groups) {
						if(group.equals(d.getName())) {
							tempTags.add(entry.tag);
							break;
						}
					}
				}
				Asset asset;
				if(!tempTags.isEmpty()) {
					tags = new String[tempTags.size()];
					tempTags.toArray(tags);
					asset = new DownloadableAsset(tags);
				} else {
					asset = new Asset();
				}

				File c = new File(d, "Contents.json");
				if(!c.exists()) {
					try {
						FileOutputStream fos = new FileOutputStream(c);
						String json = gson.toJson(asset);
						fos.write(json.getBytes());
						fos.close();
					} catch (IOException e) {
						e.printStackTrace();
						System.exit(2);
					}
				}
			}
		} else {
			placeFolderInfoIntoTheseFolders(dirs);
		}
	}

	// region getPath

	private String getPath(File file) {
		return getPath(file.getPath());
	}

	private String getPath(String filePath) {
		int index = filePath.indexOf("/");
		return filePath.substring(index + 1);
	}

	// endregion

	private String getName(String fileName) {
		String nameWithExt = getNameWithExt(fileName);
		int dotIndex = nameWithExt.lastIndexOf(".");
		if(dotIndex != -1) {
			return nameWithExt.substring(0, dotIndex);
		} else return nameWithExt;
	}

	private String getNameWithExt(String filePath) {
		int slashIndex = filePath.lastIndexOf("/");
		if(slashIndex != -1) {
			return filePath.substring(slashIndex + 1);
		} else {
			return filePath;
		}
	}

	private String getNameWithExtAndIdiom(String filePath, String idiom) {
		return idiom + "." + getNameWithExt(filePath);

	}
}
