package com.bergen.ho.iosassets;

import java.io.File;

public class DownloadableAsset extends Asset {

    private AssetProperties properties;

    DownloadableAsset(String[] tags) {
        super();
        this.properties = new AssetProperties(tags);
    }

    DownloadableAsset(File smallAsset, File origAsset, String[] tags) {
        super(smallAsset, origAsset);
        this.properties = new AssetProperties(tags);
    }
}
